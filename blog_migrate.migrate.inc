<?php
class BlogMigrateMigration extends Migration {
  public function __construct() {
    parent::__construct();

    $this->description = t('Import old blog posts');

    // Build query
    $query = Database::getConnection('default', 'for_migration')
    ->select('node', 'n');

    $query->leftJoin('node_revisions', 'r', 'n.vid = r.vid');
    $query->leftJoin('content_type_blog_post', 'b', 'n.vid = b.vid');
    $query->leftJoin('url_alias', 'u', "CONCAT('node/', CAST(n.nid AS CHAR)) = u.src");
    $query->leftJoin('files', 'f', 'f.fid = b.field_blog_thumbnail_fid');
    $query->leftJoin('files', 'fi', 'fi.fid = b.field_banner_image_fid');

    $query->fields('n', array('nid', 'title', 'status', 'created', 'changed'))
          ->fields('r', array('body'))
          ->fields('u', array('dst'))
          ->fields('f', array('filename'))
          ->fields('fi', array('filename'))
          ->condition('type', 'blog_post', '=');

    // Define source and destination
    $this->source = new MigrateSourceSQL($query, array(), $query->countQuery(), array('map_joinable' => FALSE));
    $node_options = MigrateDestinationNode::options('und', 'filtered_html');
    $this->destination = new MigrateDestinationNode('blog_post', $node_options);

    // Mappings
    $this->map = new MigrateSQLMap($this->machineName,
        array('nid' => array( // this field is used to connect user und profile2
                'type' => 'int',
                'unsigned' => TRUE,
                'not null' => TRUE,
                'description' => t('Node ID') // description never used
              )
             ),
        MigrateDestinationNode::getKeySchema()
    );

    $this->addSimpleMappings(array('title', 'body', 'status', 'created', 'changed'));
    $this->addFieldMapping('path', 'dst');

    // Map filefields
    $filepath = '';
    $args = MigrateFileFieldHandler::arguments($filepath, 'file_copy');

    $this->addFieldMapping('field_blog_banner', 'fi_filename')
         ->arguments($args);

    $this->addFieldMapping('field_blog_thumbnail', 'filename')
         ->arguments($args);

    $this->addFieldMapping('pathauto')
         ->defaultValue(FALSE);
  }
}
